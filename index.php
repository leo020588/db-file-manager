<?php

/**
 * Plugin Name:       DB and File Manager
 * Description:       Adminer and TinyFileManager integrated with wordpress.
 * Plugin URI:        https://gitlab.com/leo020588/db-file-manager
 * Version:           0.1.0
 * Requires at least: 5.8
 * Requires PHP:      7.4
 * Author:            Lionel Garcia
 */

( function () {
	$plugin_file = 'db-file-manager/index.php';
	$plugin_url  = plugins_url( '', $plugin_file );

	add_filter( "plugin_action_links_{$plugin_file}", function ( $links ) use ( $plugin_url ) {
		return array_merge( $links, [
			'<a href="' . $plugin_url . '/includes/load.php?tool=adminer" target="_blank">DB</a>',
			'<a href="' . $plugin_url . '/includes/load.php?tool=tinyfilemanager" target="_blank">Files</a>',
			'<a href="' . $plugin_url . '/includes/load.php?tool=tinyfilemanager-alternative" target="_blank">Files.A</a>',
		] );
	} );
} )();
