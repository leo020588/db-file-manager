<?php

$file = isset( $_GET['f'] ) ? $_GET['f'] : null;

$assets = require __DIR__ . '/assets.php';

if ( ! $file || ! isset( $assets[ $file ] ) ) {
	header( 'HTTP/1.0 404 Not Found');
	exit;
}

$content_types = [
	'css'   => 'text/css; charset=utf-8',
	'js'    => 'application/javascript; charset=utf-8',
	'woff2' => 'font/woff2',
];

$ext = strtolower( preg_replace( '/.+\.(\w+)$/', '${1}', $file ) );

header( 'Content-Type: ' . $content_types[ $ext ], true );

echo base64_decode( $assets[ $file ] );
