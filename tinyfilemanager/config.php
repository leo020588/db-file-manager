<?php

$default_timezone = 'America/Lima';

$auth_users = [
	'admin' => '$2y$10$mXgKIklo9iEnvDyrTKRSQeg/X7q/sFDo0F3htcRlLcZMipNf/.IzW',
];

$use_highlightjs   = true;
$highlightjs_style = 'vs';
