<?php

if ( php_sapi_name() !== 'cli' ) {
	exit;
}

require __DIR__ . '/config.php';


$assets_lists = [
	'https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js',
	'https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js',

	'https://cdn.datatables.net/plug-ins/1.11.5/sorting/absolute.js',

	'https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css',
	'https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.min.js',

	'https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.6.0/highlight.min.js',
	'https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.6.0/styles/vs.min.css',

	'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css',
	'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/fonts/fontawesome-webfont.woff2?v=4.7.0',
	// 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/fonts/fontawesome-webfont.woff?v=4.7.0',
	// 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/fonts/fontawesome-webfont.eot?v=4.7.0',
	// 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/fonts/fontawesome-webfont.eot?#iefix&v=4.7.0',
	// 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/fonts/fontawesome-webfont.ttf?v=4.7.0',
	// 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/fonts/fontawesome-webfont.svg?v=4.7.0#fontawesomeregular',

	'https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css',
	'https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js',

	'https://cdnjs.cloudflare.com/ajax/libs/ace/1.4.12/ace.js',
	// 'https://cdnjs.cloudflare.com/ajax/libs/ace/1.4.12/theme-monokai.js',
	// 'https://cdnjs.cloudflare.com/ajax/libs/ace/1.4.12/mode-php.js',
	// 'https://cdnjs.cloudflare.com/ajax/libs/ace/1.4.12/worker-php.js',
];

$assets = [];


function buildFileName ( $url ) {
	$obj = parse_url( $url );

	$filename = preg_replace( ':.+/([^/]+)$:', '${1}', $obj['path'] );
	$filename = substr( md5( $url ), 0, 12 ) . "--{$filename}";

	return $filename;
}


function fixFontAwesomeAssets ( $css, $urls ) {
	foreach ( $urls as $url ) {
		if ( strpos( $url, 'fontawesome' ) !== false ) {
			$font = buildFileName( $url );
			$css  = str_replace( '../fonts/', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/fonts/', $css );
			$css  = str_replace( $url, 'asset.php?f=' . urlencode( $font ), $css );
		}
	}

	return $css;
}


$file         = __DIR__ . '/tinyfilemanager-2.4.8.php';
$file_patched = __DIR__ . '/tinyfilemanager-2.4.8.alternative.php';
$file_content = file_get_contents( $file );

$file_content = str_replace(
	'<?php echo FM_HIGHLIGHTJS_STYLE ?>',
	$highlightjs_style,
	$file_content
);

foreach ( $assets_lists as $url ) {
	$asset   = buildFileName( $url );
	$content = file_get_contents( $url );

	if ( strpos( $url, 'font-awesome.min.css' ) !== false ) {
		$content = fixFontAwesomeAssets( $content, $assets_lists );
	}

	$file_content = str_replace( $url, 'asset.php?f=' . urlencode( $asset ), $file_content );

	$assets[ $asset ] = base64_encode( $content );
}

file_put_contents( __DIR__ . '/assets.php', '<?php return ' . var_export( $assets, true ) . ';' );

file_put_contents( $file_patched, $file_content );
