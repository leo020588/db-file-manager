<?php

require __DIR__ . '/../../../../wp-load.php';

global $current_user;

$current_user = wp_get_current_user();

$tools = [
	'adminer'                     => 'adminer',
	'tinyfilemanager'             => 'tinyfilemanager',
	'tinyfilemanager-alternative' => 'tinyfilemanager',
];

if (
	! user_can( $current_user, 'administrator' ) ||
	(
		! isset( $_GET['tool'] ) ||
		! isset( $tools[ $_GET['tool'] ] )
	)
) {
	header('HTTP/1.0 404 Not Found');
	exit;
}

$tool   = $_GET['tool'];
$folder = $tools[$tool];
$target = "../{$folder}/{$tool}.php";

if ( $folder === 'adminer' ) {
	$params = [
		'auth[driver]'   => 'server',
		'auth[server]'   => DB_HOST,
		'auth[username]' => DB_USER,
		'auth[password]' => DB_PASSWORD,
		'auth[db]'       => DB_NAME,
	];
}

if ( $folder === 'tinyfilemanager' ) {
	$params = [
		'fm_usr' => 'admin',
		'fm_pwd' => 'uTznByPEK0NKjE4zma3b',
	];
}

header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate"); // HTTP/1.1
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache"); // HTTP/1.0
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
</head>
<body>
<form action="<?=$target?>" style="display:none" method="POST">
<?php foreach( $params as $k => $v) { ?>
<input type="hidden" name="<?=$k?>" value="<?=$v?>">
<?php } ?>
</form>
<script>document.querySelector('form').submit();</script>
</body>
</html>
