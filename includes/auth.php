<?php

$required_cookies = [
	'wordpress_logged_in' => [ 'token' => null, 'valid' => false ],
	'wordpress_sec'       => [ 'token' => null, 'valid' => false ],
];

foreach ( $required_cookies as $prefix => $v ) {
	foreach ( $_COOKIE as $cookie_name => $v ) {
		if ( preg_match( "/^{$prefix}_(\S{32})$/", $cookie_name, $matches ) ) {
			$required_cookies[$prefix] = [ 'token' => $matches[1], 'valid' => true ];
			break;
		}
	}
}

if (
	! $required_cookies['wordpress_logged_in']['valid'] ||
	! $required_cookies['wordpress_sec']['valid'] ||
	$required_cookies['wordpress_logged_in']['token'] !==
	$required_cookies['wordpress_sec']['token']
) {
	header('HTTP/1.0 404 Not Found');
	exit;
}
